# Planets icon

Vector icon for Planets, a simple interactive orbital simulator written in OCaml.

The icon which conists of the SVG’s XML source code and the rendered visual output.

# Licensing

Copyright © 2018 Markus S. <kamikazow@opensuse.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License ("GPL")
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

Alternatively, the contents of this file may be used under the terms
of the Creative Commons Attribution-ShareAlike 4.0 International license
("CC BY-SA 4.0"), in which case the provisions of CC BY-SA 4.0 License
are applicable instead of those above. If you wish to allow use of your
version of this file only under the terms of the CC BY-SA 4.0 License and
not to allow others to use your version of this file under the GPL,
indicate your decision by deleting the provisions above and replace them
with the notice and other provisions required by the CC BY-SA 4.0 License.
If you do not delete the provisions above, a recipient may use your
version of this file under either the GPL or the CC BY-SA 4.0 License."

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
